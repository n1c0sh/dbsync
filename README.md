# Coding test project. 

Goal: create java console application for syncing data with database.

## Building
### Unix

Make sure *mvnw* file in root folder is executable: 
```bash
chmod +ugx ./mvnw
```

after that run following command to build jar file:
```bash
./mvnw clean package
```
### Windows

Run following command as administrator to build jar file:
```bash
mvnw.cmd clean package
```

## Running
Execute any of the following commands to test different scenarios:
```bash
java -jar ./target/dbsync-1.0.0.jar export testExport.json
java -jar ./target/dbsync-1.0.0.jar import testFileInitial.json
java -jar ./target/dbsync-1.0.0.jar import testFileWithCreateUpdateDelete.json
java -jar ./target/dbsync-1.0.0.jar import testFileWithDuplicates.json
java -jar ./target/dbsync-1.0.0.jar import testFileWithTooLongDescription.json
```

