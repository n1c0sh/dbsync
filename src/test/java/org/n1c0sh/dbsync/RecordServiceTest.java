package org.n1c0sh.dbsync;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.toomuchcoding.jsonassert.JsonAssertion.assertThatJson;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.jdbc.JdbcTestUtils.*;
import static org.springframework.util.ResourceUtils.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class RecordServiceTest {

    private static final String TEST_TABLE = "dep_table";

    @Autowired
    private JdbcTemplate jdbc;

    @Autowired
    private RecordService recordService;

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Test
    public void importFromFileToDb_importsInitialDataCorrectly() throws IOException {
        //Given:
        final File testFile = getFile("classpath:data/testFileInitial.json");

        //When:
        recordService.importFromFileToDb(testFile.getAbsolutePath());

        //Then:
        collector.checkThat(countRowsInTable(jdbc, TEST_TABLE), equalTo(9));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'PROTOSS'"), equalTo(3));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'TERRAN'"), equalTo(3));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'ZERG'"), equalTo(3));
    }

    @Test
    public void importFromFileToDb_correctlyCreatesUpdatesAndDeletesRecords() throws IOException {
        //Given:
        final File testFile = getFile("classpath:data/testFileWithCreateUpdateDelete.json");

        //When:
        recordService.importFromFileToDb(testFile.getAbsolutePath());

        //Then:
        collector.checkThat(countRowsInTable(jdbc, TEST_TABLE), equalTo(6));

        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'PROTOSS'"), equalTo(2));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'TERRAN'"), equalTo(2));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'ZERG'"), equalTo(2));

        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'PROTOSS' and dep_job = 'CARRIER' and description = 'En taro Adun!'"), equalTo(1));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'PROTOSS' and dep_job = 'SCOUT' and description = 'Scout warp successfull!'"), equalTo(1));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'PROTOSS' and dep_job = 'ARBITER'"), equalTo(0));

        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'TERRAN' and dep_job = 'WRAITH' and description = 'Transmit coordinates.'"), equalTo(1));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'TERRAN' and dep_job = 'BATTLECRUISER'"), equalTo(0));


        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'ZERG' and dep_job = 'MUTALISK' and description = 'Muaaaaaaahhh!'"), equalTo(1));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'ZERG' and dep_job = 'LEVIATHAN' and description = 'Mothership.'"), equalTo(1));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'ZERG' and dep_job = 'GUARDIAN'"), equalTo(0));
        collector.checkThat(countRowsInTableWhere(jdbc, TEST_TABLE, "dep_code = 'ZERG' and dep_job = 'OVERLORD'"), equalTo(0));

        recordService.printDbContents();
    }

    @Test(expected = java.lang.IllegalStateException.class)
    public void importFromFileToDb_throwsExceptionWhenThereAreDuplicatesInFile() throws IOException {
        //Given:
        final File testFile = getFile("classpath:data/testFileWithDuplicates.json");

        //When:
        recordService.importFromFileToDb(testFile.getAbsolutePath());

        //Then:
        //Exception
    }


    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    public void importFromFileToDb_throwsExceptionWhenDepCodeIsTooLong() throws IOException {
        //Given:
        final File testFile = getFile("classpath:data/testFileWithTooLongDepCode.json");

        //When:
        recordService.importFromFileToDb(testFile.getAbsolutePath());

        //Then:
        //Exception
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    public void importFromFileToDb_throwsExceptionWhenDepJobIsTooLong() throws IOException {
        //Given:
        final File testFile = getFile("classpath:data/testFileWithTooLongDepJob.json");

        //When:
        recordService.importFromFileToDb(testFile.getAbsolutePath());

        //Then:
        //Exception
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    public void importFromFileToDb_throwsExceptionWhenDescriptionIsTooLong() throws IOException {
        //Given:
        final File testFile = getFile("classpath:data/testFileWithTooLongDescription.json");

        //When:
        recordService.importFromFileToDb(testFile.getAbsolutePath());

        //Then:
        //Exception
    }

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void exportFromDbToFile() throws IOException {
        //Given:
        final File tempFile = tempFolder.newFile("tempFile.txt");
        //When:
        recordService.exportFromDbToFile(tempFile.getAbsolutePath());

        //Then:
        DocumentContext json = JsonPath.parse(tempFile);
        assertThatJson(json).arrayField().hasSize(9);
        assertThatJson(json).arrayField().elementWithIndex(0)
                .field("depCode").isEqualTo("PROTOSS")
                .field("depJob").isEqualTo("CARRIER")
                .field("description").isEqualTo("Capital ship fighting with pilotless interceptors");
    }
}