package org.n1c0sh.dbsync;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class DbSyncApplication {

    private static Logger LOG = LoggerFactory
            .getLogger(DbSyncApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DbSyncApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(RecordService service) {
        return args -> {
            if(args.length != 2) {
                printUsageInfo();
                return;
            }
            String command = args[0];
            switch(command) {
                case "import":
                    service.importFromFileToDb(args[1]);
                    break;
                case "export":
                    service.exportFromDbToFile(args[1]);
                    break;
                default:
                    printUsageInfo();
            }
        };
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    private void printUsageInfo() {
        LOG.warn("Usage: java -jar dbsync.jar import|export <filename>");
    }
}
