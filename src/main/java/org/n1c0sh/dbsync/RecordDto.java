package org.n1c0sh.dbsync;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class RecordDto {
    private String depCode;
    private String depJob;
    private String description;
}
