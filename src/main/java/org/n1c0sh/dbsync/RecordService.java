package org.n1c0sh.dbsync;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecordService {
    private static Logger LOG = LoggerFactory.getLogger(RecordService.class);
    private static Logger DETAILED_LOG = LoggerFactory.getLogger("detailed-log");

    private final RecordRepository repository;
    private final ModelMapper modelMapper;

    public RecordService(RecordRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    @Transactional(rollbackFor = {Exception.class})
    public void importFromFileToDb(String fileName) throws IOException{
        LOG.info("Syncing data from {}", fileName);
        DETAILED_LOG.info("Parsing data from file {}", fileName);
        List<RecordDto> parsedDataFromFile = new ObjectMapper()
                .readerFor(new TypeReference<List<RecordDto>>() {})
                .readValue(new File(fileName));
        DETAILED_LOG.info("Checking for duplicates...");
        //The following code will throw IllegalStateException in case there are duplicates in file.
        Map<Pair<String, String>, RecordDto> dataToSync = parsedDataFromFile.stream().collect(
                Collectors.toMap(
                        dto -> Pair.of(dto.getDepCode(), dto.getDepJob()),
                        dto -> dto
                ));
        DETAILED_LOG.info("Fetching data from DB...");
        Map<Pair<String, String>, Record> dataFromDb =
                repository.findAll().stream().collect(
                        Collectors.toMap(
                                record -> Pair.of(record.getDepCode(), record.getDepJob()),
                                record -> record
                        ));

        List<Record> recordsToDelete = new ArrayList<>();

        for(Pair key: dataFromDb.keySet()) {
            RecordDto dto = dataToSync.get(key);
            Record entity = dataFromDb.get(key);

            if(dto != null) {
                //this should be moved to separate function in case of more complex checks
                if(!entity.getDescription().equals(dto.getDescription())) {
                    DETAILED_LOG.info("Updating record: {}", entity);
                    entity.setDescription(dto.getDescription()); //update entry since it is present in file.
                }
                dataToSync.remove(key);
            } else {
                DETAILED_LOG.info("Queuing record for removal: {}", entity);
                recordsToDelete.add(entity);
            }
        }

        if(recordsToDelete.size() > 0) {
            DETAILED_LOG.info("Deleting obsolete records: {}", recordsToDelete);
            repository.deleteInBatch(recordsToDelete);
        }

        //Create new entries in DB since dataToSync now contains only data absent in DB.
        for(RecordDto dto: dataToSync.values()) {
            Record newRecord = repository.save(modelMapper.map(dto, Record.class));
            DETAILED_LOG.info("New record created: {}", newRecord);
        }
        LOG.info("Sync successful.");
    }

    void exportFromDbToFile(String fileName) throws IOException {
        LOG.info("Exporting database to file {}", fileName);
        DETAILED_LOG.info("Attempting to create file with name {}", fileName);
        File exportFile = new File(fileName);

        DETAILED_LOG.info("Fetching data from DB...");
        List<RecordDto> records = repository.findAll().stream()
                .map(record -> modelMapper.map(record, RecordDto.class))
                .collect(Collectors.toList());

        DETAILED_LOG.info("Writing data to file...");
        new ObjectMapper()
                .writerWithDefaultPrettyPrinter()
                .writeValue(exportFile, records);

        LOG.info("Export successful.");
    }

    void printDbContents() {
        repository.findAll().forEach(System.out::println);
    }
}
