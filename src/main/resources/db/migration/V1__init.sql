create table dep_table
(
    id BIGINT IDENTITY,
    dep_code varchar(20),
    dep_job varchar(100),
    description varchar(255),
    constraint table_name_pk_2
        unique (dep_code, dep_job)
);

INSERT into dep_table (id, dep_code, dep_job, description)
values
 (default, 'PROTOSS', 'CARRIER', 'Capital ship fighting with pilotless interceptors'),
 (default, 'PROTOSS', 'SCOUT', 'Light tactical ship for sudden strikes'),
 (default, 'PROTOSS', 'ARBITER', 'Medium ship providing invisibility to the fleet and tactical advantage'),
 (default, 'TERRAN', 'BATTLECRUISER', 'Capital ship with heavy lasers and main bean weapon'),
 (default, 'TERRAN', 'WRAITH', 'Light tactical ship with cloaking capabilities'),
 (default, 'TERRAN', 'DROPSHIP', 'General purpose dropship'),
 (default, 'ZERG', 'MUTALISK', 'Light tactical unit for sudden strikes'),
 (default, 'ZERG', 'GUARDIAN', 'Air-to-Surface unit'),
 (default, 'ZERG', 'OVERLORD', 'Control providing ship with sensors and dropship capabilities')
 ;
